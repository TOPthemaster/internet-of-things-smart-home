package com.example.admin.mqtt;

import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class TvActivity extends AppCompatActivity implements View.OnClickListener {
    private Button power;
    private Button sound_up;
    private Button sound_down;
    private Button up;
    private Button down;
    private Button left;
    private Button right;
    private Button ok;
    private Button home;
    private Button back;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_control);

        power=findViewById(R.id.power);
        sound_down=findViewById(R.id.sound_down);
        sound_up=findViewById(R.id.sound_up);
        up=findViewById(R.id.up);
        down=findViewById(R.id.down);
        left=findViewById(R.id.left);
        right=findViewById(R.id.right);
        ok=findViewById(R.id.ok);
        home=findViewById(R.id.home);
        back=findViewById(R.id.back);
        power.setOnClickListener(this);
        sound_up.setOnClickListener(this);
        sound_down.setOnClickListener(this);
        up.setOnClickListener(this);
        down.setOnClickListener(this);
        left.setOnClickListener(this);
        right.setOnClickListener(this);
        ok.setOnClickListener(this);
        home.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        //获取组件的资源id
        int id = v.getId();
        switch (id) {
            case R.id.power:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"power\":1}");
                break;
            case R.id.sound_up:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"sound_up\":1}");
                break;
            case R.id.sound_down:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"sound_down\":1}");
                break;
            case R.id.up:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"up\":1}");
                break;
            case R.id.down:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"down\":1}");
                break;
            case R.id.left:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"left\":1}");
                break;
            case R.id.right:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"right\":1}");
                break;
            case R.id.ok:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"ok\":1}");
                break;
            case R.id.back:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"back\":1}");
                break;
            case R.id.home:
                MainActivity.mqtt.publishmessageplus("tv_control","{\"home\":1}");

                break;

            default:
                break;
        }
    }
}
